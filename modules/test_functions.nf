process cat {
    input:
    file input_file

    output:
    file "${input_file.baseName}.contents.txt"

    script:
    """
    cat ${input_file} > ${input_file.baseName}.contents.txt
    """
}

process append{
    publishDir 'output', mode: 'copy'
    input:
    file input_file

    output:
    file "${input_file}"

    script:
    """
    echo "the end" >> ${input_file}
    """
}   